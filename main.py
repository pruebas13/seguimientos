from flask import Flask, request
import uuid
from marshmallow import Schema, fields, ValidationError, validates   #Para que funcione se debe instalar previamente
from datetime import datetime #now

#-------------------------Clase Imolko 2020-07-24 endpoints---------------------------------------------

app = Flask(__name__)

# @app.route("/")
# def hello_www():
#     return "Este Ano Si HD! con el Perfilsibirisssss _modificado x andres_2020-07-17"

# @app.route("/seguimientos", methods=['GET'])
# def get_seguimiento():
#     lista = [
#         {"Descripcion":"Llamada exitosa","Fecha":"20200717","Hora":"1400"},
#         {"Descripcion":"Llamada exitosa","Fecha":"20200717","Hora":"1800"},
#         {"Descripcion":"Llamada exitosa","Fecha":"20200717","Hora":"1900"},
#         {"Descripcion":"Llamada exitosa","Fecha":"20200717","Hora":"2000"}
#     ]
#     return str(lista), 200

# @app.route("/seguimientos", methods=['POST'])
# def post_seguimiento():
#     print(request.form) 
#     seguimiento_id = str(uuid.uuid4())
#     return seguimiento_id, 201


# @app.route("/seguimientos/<seguimiento_id>", methods=['GET'])
# def get_seguimiento_detail(seguimiento_id):
#     version = request.args.get("version")
#     return "Aqui se mostraria el detalle " + seguimiento_id + "-" + version

#-------------------------Clase Imolko 2020-07-28 marshmallow---------------------------------------------

def calcular_rango_edad(record):
    print (f"en calcular rango edad: {record}")
    edad = record.get("edad")
    if edad < 25:
        return "jovencito"
    if edad >= 25 and edad < 70:
        return "adulto"
    return "viejito"

class PersonSchema(Schema):
        nombre = fields.Str()
        apellido = fields.Str()
        fecha = fields.DateTime(format='%Y-%m-%d')
        created = fields.DateTime(format='%Y-%m-%d', default=datetime.now)                                                 #sino viene aplicar missing..dump_only=True
        edad = fields.Integer(required=True)
        idkey = fields.UUID(dump_only=True, default=uuid.uuid4)
        rango_edad = fields.Function(serialize=calcular_rango_edad)

        @validates("edad")
        def validar_edad(self, edad):
            if edad > 100:
                raise ValidationError("No se aceptan vejucos")

@app.route("/")
def hello_www():

    input_data = {'created':'2019-01-01','edad':70,'nombre':'andres', 'apellido':'Betancur', 'fecha': '1980-12-17'}

# input_data['name'] = input('Dime tu nombre? ')
# input_data['email'] = input('Digita tu email? ')
# input_data['fecha'] = input('Dinos la fecha de hoy en formato AAAA-MM-DD? ')

    schema = PersonSchema()
    print(f"input_data:{input_data}")
    person = schema.load(input_data)  #deserializo el dict
    print(f"loaded:{person}")
    result = schema.dump(person)      #serializo el dict
    print(f"dumpeado:{result}")
    return(result)